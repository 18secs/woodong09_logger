const express = require("express");
const app = express();
const VALID_API_KEY = "036d643f2bb14c7ea94e0952d377965d";

app.use(express.json());

function verifyApiKey(req, res, next) {
    const apiKey = req.headers["x-api-key"];
    if (apiKey === VALID_API_KEY) {
        return next();
    } else {
        res.status(401).send({ code: 'invalid_api_key' });
    }
}

app.get("/", (req, res) => {
    res.status(200).send({ message: "Hello stranger!" });
});

app.post("/", verifyApiKey, (req, res) => {
    if (req.body) {
        try {
            console.log(JSON.stringify({...req.body, log_type: "woodong09_applog" }));
            res.status(200).end();
        }
        catch (e) {
            console.error(e);
        }
    }
});

app.listen(process.env.PORT || 8000, () => {
    console.log(JSON.stringify({ logId: "system", message: "Logger started", eventDatetime: new Date() }));
});
